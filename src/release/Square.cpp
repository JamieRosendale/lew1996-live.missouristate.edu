#include "Square.h"

Square::Square(const length_unit side) throw(ShapeParameterException) {
    setDimensions(side);
}

length_unit Square::getSide() const {
    return Rectangle::getLength();
}

void Square::setSide(const length_unit side) {
    setDimensions(side);
}

// override
length_unit Square::getLength() const {
    return getSide();
}

void Square::setLength(const length_unit length) throw(ShapeParameterException) {
    setDimensions(length);
}

length_unit Square::getWidth() const {
    return getSide();
}

void Square::setWidth(const length_unit width) throw(ShapeParameterException) {
    setDimensions(width);
}

length_unit Square::getPerimeter() const {
    return Rectangle::getPerimeter();
}

area_unit Square::getArea() const {
    return Rectangle::getArea();
}

std::string Square::getName() const {
    return "Square";
}

void Square::setDimensions(const length_unit value) {
    Rectangle::setLength(value);
    Rectangle::setWidth(value);
}
